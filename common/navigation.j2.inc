<header class="header_sec newheader">
<div id="skip"><a href="#maincontent" class="skip">{% trans %}Skip to main content{% endtrans %}</a></div>
      	<div class="container">
		<nav class="navbar navbar-expand-md navbar-light nav_top">
			<a class="navbar-brand" href="{{ url_localized('index.html') }}"><img src="{{ url_static('images/logo.png') }}" alt="Taler Systems SA"></a>
			  <button class="navbar-toggler" 
              type="button" 
              data-toggle="collapse" 
              data-target="#navbarSupportedContent" 
              aria-controls="navbarSupportedContent" 
              aria-expanded="false" 
              aria-label="Toggle navigation">
		    <span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
	        <ul class="navbar-nav menu_sec">
             	<li><a href="{{ url_localized('index.html') }}">{% trans %}Home{% endtrans %}</a></li>
                <li><a href="{{ url_localized('electronic-cash.html') }}">{% trans %}Electronic Cash{% endtrans %}</a></li>      	
   				<li><a href="{{ url_localized('digital-currency.html') }}">{% trans %}CBDC{% endtrans %}</a></li>
   				<li><a href="{{ url_localized('tokenization.html') }}">{% trans %}Tokenization{% endtrans %}</a></li>
         		<li><a href="{{ url_localized('company.html') }}">{% trans %}Company{% endtrans %}</a></li>
         		<li><a href="{{ url_localized('insights.html') }}">{% trans %}Insights{% endtrans %}</a></li>
         		<li><a href="{{ url_localized('company.html#contact') }}">{% trans %}Contact{% endtrans %}</a></li>
                <li><a href="https://taler.net/{{ url_localized('index.html') }}" target="_blank" rel="noopener noreferrer">{% trans %}Community{% endtrans %}</a></li>
            </ul>
            <div class="nav-item dropdown">
      <button class="btn btn-light dropdown-toggle"
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true">
              <img src="{{ url_static('images/languageicon.svg') }}"
                    height="35"
                    alt="[{{lang}}]" />
                    {{ lang_full }} [{{ lang }}]
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      {% if lang != 'en' %}
        <li><a class="dropdown-item" href="{{ self_localized('en') }}">English [en]</a></li>
      {% endif %}
        {% if lang != 'de' %}
        <li><a class="dropdown-item" href="{{ self_localized('de') }}">Deutsch [de]</a></li>
      {% endif %}
        {% if lang != 'tr' %}
        <li><a class="dropdown-item" href="{{ self_localized('tr') }}">T&uuml;rk&ccedil;e [tr]</a></li>
      {% endif %}
        {% if lang != 'es' %}
        <li><a class="dropdown-item" href="{{ self_localized('es') }}">Espa&ntilde;ol [es]</a></li>
      {% endif %}

{# <!--  {% if lang != 'ar' %}
        <li><a class="dropdown-item" href="{{ self_localized('ar') }}">عربى [ar]</a></li>
      {% endif %}
      {% if lang != 'zh_Hant' %}
        <li><a class="dropdown-item" href="{{ self_localized('zh_Hant') }}">繁體中文 [zh]</a></li>
      {% endif %}
      {% if lang != 'zh_Hans' %}
        <li><a class="dropdown-item" href="{{ self_localized('zh_Hans') }}">简体中文 [zh_Hans]</a></li>
      {% endif %}
      {% if lang != 'fr' %}
        <li><a class="dropdown-item" href="{{ self_localized('fr') }}">Fran&ccedil;ais [fr]</a></li>
      {% endif %}
        {% if lang != 'hi' %}
        <li><a class="dropdown-item" href="{{ self_localized('hi') }}">हिंदी [hi]</a></li>
      {% endif %}
      {% if lang != 'it' %}
        <li><a class="dropdown-item" href="{{ self_localized('it') }}">Italiano [it]</a></li>
      {% endif %}
      {% if lang != 'ja' %}
        <li><a class="dropdown-item" href="{{ self_localized('ja') }}">日本語 [ja]</a></li>
      {% endif %}
      {% if lang != 'ko' %}
        <li><a class="dropdown-item" href="{{ self_localized('ko') }}">한국어 [ko]</a></li>
      {% endif %}
      {% if lang != 'pt' %}
        <li><a class="dropdown-item" href="{{ self_localized('pt') }}">Portugu&ecirc;s [pt]</a></li>
      {% endif %}
      {% if lang != 'pt_BR' %}
        <li><a class="dropdown-item" href="{{ self_localized('pt_BR') }}">Portugu&ecirc;s (Brazil) [pt]</a></li>
      {% endif %}
      {% if lang != 'ru' %}
        <li><a class="dropdown-item" href="{{ self_localized('ru') }}">&#x420;&#x443;&#x301;&#x441;&#x441;&#x43A;&#x438;&#x439;&#x20;&#x44F;&#x437;&#x44B;&#x301;&#x43A; [ru]</a></li>
      {% endif %}
      {% if lang != 'sv' %}
        <li><a class="dropdown-item" href="{{ self_localized('sv') }}">Svenska [sv]</a></li>
      {% endif %} --> #}
      </ul>
            </div>
        </div>
		</nav>
	</div>
</header>
