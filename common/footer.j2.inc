<footer class="footersec">
	<div class="foottop">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footbox text-center">
						<a href="{{ url_localized('index.html') }}" class="footlogo"><img src="{{ url_static('images/footlogo.png') }}" alt="Index"></a>
						<ul class="footsos">
							<!-- li><a href="#" target="_blank" rel="noopener noreferrer"><img src="{{ url_static('images/footsos1.png') }}" alt="Taler Systems SA Facebook-Channel"></a></li -->
							<li><a href="https://twitter.com/taler" target="_blank" rel="noopener noreferrer">
							<img src="{{ url_static('images/footsos2.png') }}" alt="Twitter Channel of Taler Systems SA"></a></li>
							<!-- li><a href="#" target="_blank" rel="noopener noreferrer"><img src="{{ url_static('images/footsos3.png') }}" alt="Taler Systems SA Youtube Channel"></a></li -->
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footbox footmenu">
						<h4>{% trans %}Quick Links{% endtrans %}</h4>
						<ul>
							<li><a href="{{ url_localized('index.html') }}" alt="{% trans %}Taler Systems Index Page{% endtrans %}">{% trans %}Home{% endtrans %}</a></li>
							<li><a href="{{ url_localized('electronic-cash.html') }}">{% trans %}Electronic Cash{% endtrans %}</a></li>
							<li><a href="{{ url_localized('digital-currency.html') }}">{% trans %}CBDC{% endtrans %}</a></li>
							<li><a href="{{ url_localized('tokenization.html') }}">{% trans %}Tokenization{% endtrans %}</a></li>
							<li><a href="{{ url_localized('insights.html') }}">{% trans %}Insights{% endtrans %}</a></li>
							<li><a href="{{ url_localized('company.html') }}">{% trans %}Company{% endtrans %}</a> /
							<a href="{{ url_localized('company.html#contact') }}">{% trans %}Contact{% endtrans %}</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footbox footmenu">
						<h4>{% trans %}Email Contacts{% endtrans %}</h4>
						<ul>
							<li><a href="mailto:sales'AT'taler-systems.com">{% trans %}Sales{% endtrans %}</a></li>
							<li><a href="mailto:marketing'AT'taler-systems.com">{% trans %}Marketing{% endtrans %}</a></li>
							<li><a href="mailto:press'AT'taler-systems.com">{% trans %}PR and Media Contact{% endtrans %}</a></li>
							<li><a href="mailto:invest'AT'taler-systems.com">{% trans %}Investors Contact{% endtrans %}</a></li>
							<li><a href="mailto:support'AT'taler-systems.com">{% trans %}Support{% endtrans %}</a></li>
							<li><a href="mailto:onboarding'AT'taler-systems.com">{% trans %}Onboarding{% endtrans %}</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footbox footadres">
						<h4>{% trans %}Contact Info{% endtrans %}</h4>
						<ul>
							<li>{% trans %}Taler Systems SA, Luxembourg{% endtrans %}</li>
							<li><a class="phone" href="tel:0041786926894">Tel: +41 78 692 6894</a></li>
							<li>7, Rue de Mondorf</li>
							<li>L-5421 Erpeldange, Luxembourg</li>
							<li><a href="https://taler-systems.com" target="_blank" rel="noopener noreferrer">www.taler-systems.com</a></li>
							<li><a href="https://taler.net/{{ url_localized('index.html') }}" target="_blank" rel="noopener noreferrer">{% trans %}Community{% endtrans %}: www.taler.net</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footbottm">
		<div class="container">
			<div class="footbottminn">
				<p>© {% trans %}Copyright{% endtrans %} 2021-2025   |   <a href="{{ url_localized('index.html') }}">Taler Systems SA</a>   |   {% trans %}All Rights Reserved{% endtrans %}</p>
				<p>{% trans %}Terms of Service / Privacy Policy: We do not assume any liability for the correctness of contents. We do not collect any data and do not use cookies.
				<br>Our web server generates a log file with IP addresses of visitors. However, these are evaluated exclusively for troubleshooting or problem mitigation.{% endtrans %}</p>
				<p><a href="https://www.gnu.org/licenses/fdl-1.3.html.en" target="_blank" rel="noopener noreferrer">{% trans %}License{% endtrans %}: GNU Free Documentation License</a></p>
			</div>
		</div>
	</div>
